package com.golonko.clinic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;

@SpringBootApplication
public class ApplicationRepositoryTests implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(ClinicDemoDatabaseApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ClinicDemoDatabaseApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        logger.info("Starting application on {}", LocalDateTime.now());

    }


}
