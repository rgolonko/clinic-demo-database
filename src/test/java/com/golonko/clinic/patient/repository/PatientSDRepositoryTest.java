package com.golonko.clinic.patient.repository;

import com.golonko.clinic.ApplicationRepositoryTests;
import com.golonko.clinic.patient.entity.Patient;
import com.golonko.clinic.visit.entity.Status;
import com.golonko.clinic.visit.entity.Visit;
import org.hibernate.LazyInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = ApplicationRepositoryTests.class)
@ActiveProfiles("test")
public class PatientSDRepositoryTest {

    @Autowired
    private PatientSDRepository patientSDRepository;

    @Test
    public void returnDetailsWhenPatientExists() {
        Patient patient = getPatientIfExist();
        assertEquals("First name is not equal", "Gabriel", patient.getFirstName());
        assertEquals("Last name is not equal", "Pawlak", patient.getLastName());
        assertEquals("PESEL is not equal", 99050338564L, patient.getPesel());
        assertTrue("Active is not equal", patient.isActive());
    }

    @Test(expected = NoSuchElementException.class)
    public void failWhenPatientDoesntExist() {
        patientSDRepository.findByIdOrThrow(100L);
    }

    @Test(expected = NoSuchElementException.class)
    public void failWhenPatientWithVisitDoesntExist() {
        patientSDRepository.findWithVisitsByIdOrThrow(100L);
    }

    @Test
    public void visitsReturnedWithPatientWhenAssigned() {
        Optional<Patient> patientOptional = patientSDRepository.findWithVisitsById(1L);
        assertThat(patientOptional).isPresent();
        Patient patient = patientOptional.get();

        // Junit
        assertEquals("First name is not equal", "Gabriel", patient.getFirstName());
        assertEquals("Last name is not equal", "Pawlak", patient.getLastName());
        assertEquals("PESEL is not equal", 99050338564L, patient.getPesel());
        assertTrue("Active is not equal", patient.isActive());
        // AssertJ
        assertThat(patient.getFirstName()).isEqualToIgnoringCase("gabriel");
        assertThat(patient.getLastName()).isEqualToIgnoringCase("pawlak");
        assertThat(patient.getPesel()).isEqualTo(99050338564L);
        assertThat(patient.isActive()).isTrue();

        List<Visit> visitList = patient.getVisits();
        visitList.sort(Comparator.comparingLong(Visit::getId));
        Visit visit = visitList.get(0);

        // Junit
        assertEquals(Status.PENDING, visit.getStatus());
        assertEquals(LocalDateTime.of(2019, 10, 22, 12, 0, 0), visit.getDate());
        assertFalse(visitList.isEmpty());
        assertEquals(5, visitList.size());
        // AssertJ
        assertThat(visitList)
                .isNotEmpty()
                .hasSize(5);
        Visit visitExpected = new Visit(LocalDateTime.of(2019, 10, 22, 12, 0, 0), Status.PENDING);
        assertThat(visit).isEqualToComparingOnlyGivenFields(visitExpected, "date", "status");
    }

    @Test(expected = LazyInitializationException.class)
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void failWhenNoVisitsReturnedWithPatientWhenNoAssigned() {
        Patient patient = getPatientIfExist();
        patient.getVisits().size();
    }

    @Test
    public void returnAllPatients() {
        int noOfPatients = 4;
        assertThat(patientSDRepository.findAll()).hasSize(noOfPatients);
    }

    @Test
    public void createNewPatient() {
        String firstName = "Tomasz";
        String lastName = "Tomaszewski";
        long pesel = 88022112345L;
        Patient newPatient = new Patient(firstName, lastName, pesel);

        patientSDRepository.saveAndFlush(newPatient);
        Optional<Patient> savedPatient = patientSDRepository.findById(newPatient.getId());
        assertThat(savedPatient)
                .isPresent()
                .get()
                .isEqualToComparingOnlyGivenFields(newPatient, "firstName", "lastName", "pesel", "active");
    }

    private Patient getPatientIfExist() {
        return patientSDRepository.findById(1L).orElseThrow(() -> new NoSuchElementException("Patient with given id " +
                "doesn't exist"));
    }
}