INSERT INTO doctor (first_name, last_name, licence_number) VALUES ('Edyta', 'Socha', 115);
INSERT INTO doctor (first_name, last_name, licence_number) VALUES ('Filip', 'Kosiński', 225);
INSERT INTO doctor (first_name, last_name, licence_number) VALUES ('Marta', 'Jaworska', 383);

INSERT INTO patient (first_name, last_name, pesel) VALUES ('Gabriel', 'Pawlak', 99050338564);
INSERT INTO patient (first_name, last_name, pesel) VALUES ('Wiktoria', 'Nowak', 84053138723);
INSERT INTO patient (first_name, last_name, pesel) VALUES ('Kacper', 'Lewandowski', 72102291369);
INSERT INTO patient (first_name, last_name, pesel) VALUES ('Monika', 'Zukowska', 61042089396);

INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-22 12:00:00', 'PENDING', 1, 1);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-22 13:00:00', 'NOT_ASSIGNED', NULL, 2);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-30 14:00:00', 'PENDING', 1, 3);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-22 13:00:00', 'DONE', 2, 1);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-22 16:00:00', 'NOT_ASSIGNED', NULL, 4);

INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-23 12:00:00', 'PENDING', 2, 2);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-23 13:00:00', 'DONE', 3, 1);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-23 13:00:00', 'PENDING', 1, 4);

INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-22 08:00:00', 'CANCELED', 1, 1);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-24 09:00:00', 'NOT_ASSIGNED', NULL, 2);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-24 08:00:00', 'PENDING', 2, 2);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-21 09:00:00', 'CANCELED', 3, 1);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-24 10:00:00', 'NOT_ASSIGNED', NULL, 2);
INSERT INTO visit (date, status, doctor_id, patient_id) VALUES ('2019-10-24 10:00:00', 'PENDING', 3, 3);

INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-22', 1);
INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-23', 6);
INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-24', 10);
INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-25', 2);
INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-28', 8);
INSERT INTO prescription (expire_date, visit_id) VALUES ('2019-11-30', 9);

INSERT INTO medicine (name, code) VALUES ('Clemasolin', 'ABC123');
INSERT INTO medicine (name, code) VALUES ('Ibuprom', '789OPL');
INSERT INTO medicine (name, code) VALUES ('Cyclex Forte', '764yUJ');
INSERT INTO medicine (name, code) VALUES ('Paracetamol', 'abc456ABC');
INSERT INTO medicine (name, code) VALUES ('Morfina', 'PLjkl4502');

INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (1, 1);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (1, 2);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (1, 3);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (1, 4);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (6, 2);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (6, 3);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (6, 4);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (2, 4);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (2, 5);
INSERT INTO prescription_medicine (prescription_id, medicine_id) VALUES (8, 4);


