package com.golonko.clinic.patient.repository;

import com.golonko.clinic.patient.entity.Patient;

interface CustomizedPatientRepository {

    Patient findByIdOrThrow(Long id);

    Patient findWithVisitsByIdOrThrow(Long id);
}
