package com.golonko.clinic.patient.repository;

import com.golonko.clinic.patient.entity.Patient;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatientSDRepository extends JpaRepository<Patient, Long>, CustomizedPatientRepository {

    @EntityGraph(attributePaths = Patient.FIELD_VISITS)
    Optional<Patient> findWithVisitsById(Long id);
}
