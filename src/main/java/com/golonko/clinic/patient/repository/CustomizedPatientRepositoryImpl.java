package com.golonko.clinic.patient.repository;

import com.golonko.clinic.patient.entity.Patient;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Repository
class CustomizedPatientRepositoryImpl implements CustomizedPatientRepository {

    private final PatientSDRepository patientSDRepository;

    public CustomizedPatientRepositoryImpl(@Lazy PatientSDRepository patientSDRepository) {
        this.patientSDRepository = patientSDRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Patient findByIdOrThrow(Long id) {
        return patientSDRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(
                        String.format("Patient with %d not found.", id)
                ));
    }

    @Transactional(readOnly = true)
    @Override
    public Patient findWithVisitsByIdOrThrow(Long id) {
        return patientSDRepository.findWithVisitsById(id)
                .orElseThrow(() -> new NoSuchElementException(
                        String.format("Patient with %d not found.", id)
                ));
    }


}
