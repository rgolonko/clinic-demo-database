package com.golonko.clinic.patient.entity;

import com.golonko.clinic.base.entity.springData.BaseSDEntity;
import com.golonko.clinic.visit.entity.Visit;
import org.hibernate.annotations.NaturalId;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.golonko.clinic.patient.entity.Patient.FIELD_PESEL;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        name = "uk_patient_" + FIELD_PESEL,
        columnNames = FIELD_PESEL
))
public class Patient extends BaseSDEntity {

    public static final String FIELD_PESEL = "pesel";
    public static final String FIELD_VISITS = "visits";

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NaturalId
    @NotNull
    private long pesel;

    @NotNull
    @Column(columnDefinition = "boolean default true")
    private boolean active;

    @OneToMany(
            mappedBy = Visit.FIELD_PATIENT,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}
    )
    private List<Visit> visits;

    public Patient() {
        // no-arg constructor required by Hibernate
    }

    public Patient(String firstName, String lastName, long pesel) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getPesel() {
        return pesel;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Visit> getVisits() {
        return visits != null ? visits : Collections.emptyList();
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public void addVisit(Visit visit) {
        if (visits == null) {
            visits = new ArrayList<>();
        }
        visits.add(visit);
        visit.setPatient(this);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + this.getId() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", pesel=" + pesel +
                ", active=" + active +
                '}';
    }
}
