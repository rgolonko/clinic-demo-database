package com.golonko.clinic.patient.service;

import com.golonko.clinic.patient.entity.Patient;
import com.golonko.clinic.patient.repository.PatientSDRepository;
import com.golonko.clinic.visit.entity.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private final PatientSDRepository patientSDRepository;

    @Autowired
    public PatientServiceImpl(PatientSDRepository patientSDRepository) {
        this.patientSDRepository = patientSDRepository;
    }

    @Override
    public List<Patient> findAll() {
        return patientSDRepository.findAll();
    }

    @Override
    public Patient find(Long id) {
        return patientSDRepository.findByIdOrThrow(id);
    }

    @Override
    public Patient findWithVisits(Long id) {
        return patientSDRepository.findWithVisitsByIdOrThrow(id);
    }

    @Override
    public void create(Patient patient) {
        patientSDRepository.save(patient);
    }

    @Transactional
    @Override
    public Patient update(Long id, String firstName, String lastName) {
        final Patient patient = this.find(id);
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        return patient;
    }

    @Transactional
    @Override
    public void delete(Long id) {
        final Patient patient = this.findWithVisits(id);
        patient.setActive(false);

        patient.getVisits()
                .stream()
                .filter(visit -> (visit.getStatus() == Status.PENDING || visit.getStatus() == Status.NOT_ASSIGNED)
                        && visit.getDate().isAfter(LocalDateTime.now()))
                .forEach(visit -> visit.setStatus(Status.CANCELED));
    }

}
