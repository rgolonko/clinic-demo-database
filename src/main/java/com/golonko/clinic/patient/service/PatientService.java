package com.golonko.clinic.patient.service;

import com.golonko.clinic.patient.entity.Patient;

import java.util.List;

public interface PatientService {

    List<Patient> findAll();

    Patient find(Long id);

    Patient findWithVisits(Long id);

    void create(Patient patient);

    Patient update(Long id, String firstName, String lastName);

    void delete(Long id);

}
