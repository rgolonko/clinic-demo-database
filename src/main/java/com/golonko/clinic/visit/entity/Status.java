package com.golonko.clinic.visit.entity;

public enum Status {

    PENDING,
    CANCELED,
    DONE,
    NOT_ASSIGNED

}
