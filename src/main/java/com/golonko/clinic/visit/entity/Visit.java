package com.golonko.clinic.visit.entity;

import com.golonko.clinic.base.entity.springData.BaseSDEntity;
import com.golonko.clinic.doctor.entity.Doctor;
import com.golonko.clinic.patient.entity.Patient;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static com.golonko.clinic.visit.entity.Visit.FIELD_DOCTOR;
import static com.golonko.clinic.visit.entity.Visit.FIELD_PATIENT;
import static com.golonko.clinic.visit.entity.Visit.GRAPH_VISIT_DOCTOR;
import static com.golonko.clinic.visit.entity.Visit.GRAPH_VISIT_PATIENT_AND_DOCTOR;

@Entity
@NamedEntityGraphs({
        @NamedEntityGraph(name = GRAPH_VISIT_PATIENT_AND_DOCTOR,
                attributeNodes = {
                        @NamedAttributeNode(FIELD_PATIENT),
                        @NamedAttributeNode(FIELD_DOCTOR)
                }),
        @NamedEntityGraph(name = GRAPH_VISIT_DOCTOR,
                attributeNodes = {
                        @NamedAttributeNode(FIELD_DOCTOR)
                })
})
public class Visit extends BaseSDEntity {

    public static final String FIELD_DOCTOR = "doctor";
    public static final String FIELD_PATIENT = "patient";
    public static final String GRAPH_VISIT_PATIENT_AND_DOCTOR = "Visit_patientAndDoctor";
    public static final String GRAPH_VISIT_DOCTOR = "Visit_doctor";

    @NotNull
    private LocalDateTime date;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_visit_doctor"))
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_visit_patient"))
    private Patient patient;

    public Visit() {
        // no-arg constructor required by Hibernate
    }

    public Visit(LocalDateTime date, Status status) {
        this.date = date;
        this.status = status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

}
