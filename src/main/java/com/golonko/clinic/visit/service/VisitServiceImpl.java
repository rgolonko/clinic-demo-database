package com.golonko.clinic.visit.service;

import com.golonko.clinic.doctor.entity.Doctor;
import com.golonko.clinic.doctor.service.DoctorService;
import com.golonko.clinic.patient.entity.Patient;
import com.golonko.clinic.patient.service.PatientService;
import com.golonko.clinic.visit.entity.Status;
import com.golonko.clinic.visit.entity.Visit;
import com.golonko.clinic.visit.repository.VisitSDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitSDRepository visitSDRepository;
    private final PatientService patientService;
    private final DoctorService doctorService;

    @Autowired
    public VisitServiceImpl(VisitSDRepository visitSDRepository, PatientService patientService, DoctorService doctorService) {
        this.visitSDRepository = visitSDRepository;
        this.patientService = patientService;
        this.doctorService = doctorService;
    }

    @Override
    public List<Visit> findAll(int page, int size, String sortBy) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sortBy));
        return visitSDRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Visit> findAllByDoctorIdAndDateAndStatus(Long id, LocalDateTime localDateTime, Status status) {
        return visitSDRepository.findByDoctor_IdAndDateAfterAndStatusIs(id, localDateTime, status);
    }

    @Override
    public Visit find(Long id) {
        return visitSDRepository.findByIdOrThrow(id);
    }

    @Override
    public Visit findWithPatientAndDoctor(Long id) {
        return visitSDRepository.findWithPatientAndDoctorOrThrow(id);
    }

    @Override
    public Visit findOne(Long id) {
        try {
            return visitSDRepository.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new NoSuchElementException(
                    String.format("Visit with %d not found", id));
        }
    }

    @Transactional
    @Override
    public void create(Long patientId, Long doctorId, Visit visit) {
        final Patient patient = patientService.findWithVisits(patientId);
        final Doctor doctor = doctorService.findOne(doctorId);
        visit.setDoctor(doctor);
        patient.addVisit(visit);
    }

    @Transactional
    @Override
    public Visit update(Long id, Visit visit) {
        final Visit existingVisit = visitSDRepository.findByIdOrThrow(id);
        existingVisit.setDate(visit.getDate());
        existingVisit.setStatus(visit.getStatus());
        return existingVisit;
    }

    @Transactional
    @Override
    public void delete(Long id) {
        this.find(id).setStatus(Status.CANCELED);
    }
}
