package com.golonko.clinic.visit.service;

import com.golonko.clinic.visit.entity.Status;
import com.golonko.clinic.visit.entity.Visit;

import java.time.LocalDateTime;
import java.util.List;

public interface VisitService {

    List<Visit> findAll(int noPage, int sizePage, String sortBy);

    List<Visit> findAllByDoctorIdAndDateAndStatus(Long id, LocalDateTime localDateTime, Status status);

    Visit find(Long id);

    Visit findWithPatientAndDoctor(Long id);

    Visit findOne(Long id);

    void create(Long patientId, Long doctorId, Visit visit);

    Visit update(Long id, Visit visit);

    void delete(Long id);

}
