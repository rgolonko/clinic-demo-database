package com.golonko.clinic.visit.repository;

import com.golonko.clinic.visit.entity.Visit;

interface CustomizedVisitRepository {

    Visit findByIdOrThrow(Long id);

    Visit findWithPatientAndDoctorOrThrow(Long id);

}
