package com.golonko.clinic.visit.repository;

import com.golonko.clinic.visit.entity.Visit;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.NoSuchElementException;

@Repository
class CustomizedVisitRepositoryImpl implements CustomizedVisitRepository {

    private final VisitSDRepository visitSDRepository;

    public CustomizedVisitRepositoryImpl(@Lazy VisitSDRepository visitSDRepository) {
        this.visitSDRepository = visitSDRepository;
    }

    @Override
    public Visit findByIdOrThrow(Long id) {
        return visitSDRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(
                        String.format("Visit with %d not found.", id)
                ));
    }

    @Override
    public Visit findWithPatientAndDoctorOrThrow(Long id) {
        return visitSDRepository.findWithPatientAndDoctorById(id)
                .orElseThrow(() -> new NoSuchElementException(
                        String.format("Visit with %d not found.", id)
                ));
    }
}
