package com.golonko.clinic.visit.repository;

import com.golonko.clinic.visit.entity.Status;
import com.golonko.clinic.visit.entity.Visit;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface VisitSDRepository extends JpaRepository<Visit, Long>, CustomizedVisitRepository {

    @EntityGraph(value = Visit.GRAPH_VISIT_PATIENT_AND_DOCTOR)
    Optional<Visit> findWithPatientAndDoctorById(Long id);

    List<Visit> findByDoctor_IdAndDateAfterAndStatusIs(Long id, LocalDateTime localDateTime, Status status);


}
