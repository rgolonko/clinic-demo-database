package com.golonko.clinic.prescription.repository;

import com.golonko.clinic.prescription.entity.Prescription;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

@Repository
public class PrescriptionJpaDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public PrescriptionJpaDAO() {
        //no operation - nop
    }

    public List<Prescription> findAll() {
        return entityManager
                .createQuery("from Prescription", Prescription.class)
                .getResultList();
    }

    public List<Prescription> findAll(int page, int size) {
        return entityManager
                .createQuery("select p from Prescription p order by p.expireDate", Prescription.class)
                .setFirstResult(page * size)
                .setMaxResults(size)
                .getResultList();
    }

    public Optional<Prescription> findById(Long id) {
        return Optional.ofNullable(entityManager.find(Prescription.class, id));
    }

    public Prescription findByIdOrThrow(Long id) {
        return findById(id).orElseThrow(() -> new NoSuchElementException(
                String.format("Prescription with id %d not found.", id)
        ));
    }

    public Optional<Prescription> findWithMedicinesById(Long id) {
        final EntityGraph entityGraph = entityManager.getEntityGraph(Prescription.GRAPH_PRESCRIPTION_MEDICINES);
        final Map<String, Object> properties = Collections.singletonMap("javax.persistence.fetchgraph", entityGraph);

        return Optional.ofNullable(
                entityManager.find(Prescription.class, id, properties)
        );
    }

    public Prescription findWithMedicinesByIdOrThrow(Long id) {
        return findWithMedicinesById(id).orElseThrow(() -> new NoSuchElementException(
                String.format("Prescription with id %d not found.", id)
        ));
    }

    public void save(Prescription prescription) {
        entityManager.persist(prescription);
    }

    public void delete(Prescription prescription) {
        entityManager.remove(prescription);
    }
}
