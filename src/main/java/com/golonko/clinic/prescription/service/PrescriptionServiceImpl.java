package com.golonko.clinic.prescription.service;

import com.golonko.clinic.medicine.entity.Medicine;
import com.golonko.clinic.medicine.service.MedicineService;
import com.golonko.clinic.prescription.entity.Prescription;
import com.golonko.clinic.prescription.repository.PrescriptionJpaDAO;
import com.golonko.clinic.visit.entity.Visit;
import com.golonko.clinic.visit.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.List;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {

    private final PrescriptionJpaDAO prescriptionDAO;
    private final VisitService visitService;
    private final MedicineService medicineService;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public PrescriptionServiceImpl(PrescriptionJpaDAO prescriptionDAO, VisitService visitService, MedicineService medicineService) {
        this.prescriptionDAO = prescriptionDAO;
        this.visitService = visitService;
        this.medicineService = medicineService;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Prescription> findAll() {
        return prescriptionDAO.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Prescription> findAll(int page, int size) {
        return prescriptionDAO.findAll(page, size);
    }

    @Transactional(readOnly = true)
    @Override
    public Prescription find(Long id) {
        return prescriptionDAO.findByIdOrThrow(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Prescription findWithMedicines(Long id) {
        return prescriptionDAO.findWithMedicinesByIdOrThrow(id);
    }

    @Transactional
    @Override
    public void create(Long visitId, List<Long> medicinesId, LocalDate expireDate) {
        final List<Medicine> medicines = medicineService.findWithPrescriptionsByMultipleId(medicinesId);
        final Visit visit = visitService.findOne(visitId);
        final Prescription prescription = new Prescription(expireDate, visit);
        medicines.forEach(prescription::addMedicine);

        prescriptionDAO.save(prescription);
    }

    @Transactional
    @Override
    public Prescription update(Long id, LocalDate expireDate) {
        final Prescription prescription = prescriptionDAO.findByIdOrThrow(id);
        prescription.setExpireDate(expireDate);
        return prescription;
    }

    @Transactional
    @Override
    public void delete(Long id) {
        final Prescription prescription = this.find(id);
        prescriptionDAO.delete(prescription);
    }

}
