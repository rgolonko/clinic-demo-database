package com.golonko.clinic.prescription.service;

import com.golonko.clinic.prescription.entity.Prescription;

import java.time.LocalDate;
import java.util.List;

public interface PrescriptionService {

    List<Prescription> findAll();

    List<Prescription> findAll(int page, int size);

    Prescription find(Long id);

    Prescription findWithMedicines(Long id);

    void create(Long visitId, List<Long> medicinesId, LocalDate expireDate);

    Prescription update(Long id, LocalDate expireDate);

    void delete(Long id);

}
