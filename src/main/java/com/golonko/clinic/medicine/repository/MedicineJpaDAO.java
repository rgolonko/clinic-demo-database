package com.golonko.clinic.medicine.repository;

import com.golonko.clinic.medicine.entity.Medicine;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Repository
public class MedicineJpaDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public MedicineJpaDAO() {
    }

    public List<Medicine> findAll() {
        return entityManager
                .createQuery("from Medicine", Medicine.class)
                .getResultList();
    }


    public Optional<Medicine> findById(Long id) {
        return Optional.ofNullable(
                entityManager
                        .createQuery("select m " +
                                "from Medicine m " +
                                "where m.id=:id", Medicine.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    public Medicine findByIdOrThrow(Long id) {
        return findById(id).orElseThrow(() -> new NoSuchElementException(
                String.format("Medicine with id %d not found.", id)
        ));
    }

    public List<Medicine> findWithPrescriptionsByMultipleId(List<Long> listId) {
        return entityManager
                .createQuery("select m " +
                        "from Medicine m " +
                        "join fetch m.prescriptions p " +
                        "where m.id in (:id)", Medicine.class)
                .setParameter("id", listId)
                .getResultList();
    }

    public Optional<Medicine> findWithPrescriptionsById(Long id) {
        return Optional.ofNullable(
                entityManager
                        .createQuery("select m " +
                                "from Medicine m " +
                                "join fetch m.prescriptions p " +
                                "where m.id=:id", Medicine.class)
                        .setParameter("id", id)
                        .getSingleResult()
        );
    }

    public Medicine findWithPrescriptionsByIdOrThrow(Long id) {
        return findWithPrescriptionsById(id).orElseThrow(() -> new NoSuchElementException(
                String.format("Medicine with id %d not found.", id)
        ));
    }

    public void save(Medicine medicine) {
        entityManager.persist(medicine);
    }

    public void deleteById(Long id) {
        entityManager
                .createQuery("delete from Medicine m " +
                        "where m.id=:id")
                .setParameter("id", id)
                .executeUpdate();
    }

}