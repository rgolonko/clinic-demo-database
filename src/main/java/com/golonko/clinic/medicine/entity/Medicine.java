package com.golonko.clinic.medicine.entity;

import com.golonko.clinic.base.entity.jpa.Audit;
import com.golonko.clinic.prescription.entity.Prescription;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static com.golonko.clinic.medicine.entity.Medicine.FIELD_CODE;
import static com.golonko.clinic.medicine.entity.Medicine.FIELD_PRESCRIPTIONS;
import static com.golonko.clinic.medicine.entity.Medicine.GRAPH_MEDICINE_PRESCRIPTIONS;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        name = "uk_medicine_code",
        columnNames = FIELD_CODE
))
@NamedEntityGraph(name = GRAPH_MEDICINE_PRESCRIPTIONS,
        attributeNodes = {
                @NamedAttributeNode(FIELD_PRESCRIPTIONS)
        }
)
public class Medicine {

    public static final String FIELD_CODE = "code";
    public static final String FIELD_PRESCRIPTIONS = "prescriptions";
    public static final String GRAPH_MEDICINE_PRESCRIPTIONS = "Medicine.prescriptions";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NaturalId
    @NotNull
    private String code;

    @ManyToMany(mappedBy = Prescription.FIELD_MEDICINES)
    private Set<Prescription> prescriptions = new HashSet<>();

    // FIXME: NPE fix https://hibernate.atlassian.net/browse/HHH-13110
    @Embedded
    private Audit audit = new Audit();

    private Medicine() {
        // no-arg constructor required by Hibernate
        // private constructor to force setting "code" from parameter constructor
    }

    public Medicine(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Audit getAudit() {
        return audit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medicine)) {
            return false;
        }
        Medicine medicine = (Medicine) o;
        return code.equals(medicine.code);
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
