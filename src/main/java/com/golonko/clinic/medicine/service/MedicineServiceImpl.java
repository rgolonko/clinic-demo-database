package com.golonko.clinic.medicine.service;

import com.golonko.clinic.medicine.entity.Medicine;
import com.golonko.clinic.medicine.repository.MedicineJpaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService {

    private final MedicineJpaDAO medicineDAO;

    @Autowired
    public MedicineServiceImpl(MedicineJpaDAO medicineDAO) {
        this.medicineDAO = medicineDAO;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Medicine> findAll() {
        return medicineDAO.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Medicine find(Long id) {
        return medicineDAO.findByIdOrThrow(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Medicine> findWithPrescriptionsByMultipleId(List<Long> medicinesId) {
        return medicineDAO.findWithPrescriptionsByMultipleId(medicinesId);
    }

    @Transactional(readOnly = true)
    @Override
    public Medicine findWithPrescription(Long id) {
        return medicineDAO.findWithPrescriptionsByIdOrThrow(id);
    }

    @Transactional
    @Override
    public void create(String name, String code) {
        final Medicine medicine = new Medicine(name, code);
        medicineDAO.save(medicine);
    }

    @Transactional
    @Override
    public Medicine update(Long id, String name) {
        final Medicine medicine = medicineDAO.findByIdOrThrow(id);
        medicine.setName(name);
        return medicine;
    }

    @Transactional
    @Override
    public void delete(Long id) {
        final Medicine medicine = this.findWithPrescription(id);
        medicine.getPrescriptions().forEach(p -> p.removeMedicine(medicine));
        medicineDAO.deleteById(id);
    }
}
