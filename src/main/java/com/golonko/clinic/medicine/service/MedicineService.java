package com.golonko.clinic.medicine.service;

import com.golonko.clinic.medicine.entity.Medicine;

import java.util.List;

public interface MedicineService {

    List<Medicine> findAll();

    Medicine find(Long id);

    List<Medicine> findWithPrescriptionsByMultipleId(List<Long> listId);

    Medicine findWithPrescription(Long id);

    void create(String name, String code);

    Medicine update(Long id, String name);

    void delete(Long id);

}
