package com.golonko.clinic.base.entity.jpa;

import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Embeddable
public class Audit {

    private LocalDateTime createdOn;

    private LocalDateTime updatedOn;

    public Audit() {
        // no-arg constructor required by Hibernate
    }

    @PrePersist
    private void preCreate() {
        this.createdOn = LocalDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        this.updatedOn = LocalDateTime.now();
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }
}
