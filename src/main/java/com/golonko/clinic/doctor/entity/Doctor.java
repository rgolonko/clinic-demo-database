package com.golonko.clinic.doctor.entity;

import com.golonko.clinic.base.entity.springData.BaseSDEntity;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import static com.golonko.clinic.doctor.entity.Doctor.FIELD_LICENCE_NUMBER;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        name = "uk_doctor_" + FIELD_LICENCE_NUMBER,
        columnNames = FIELD_LICENCE_NUMBER
))
public class Doctor extends BaseSDEntity {

    static final String FIELD_LICENCE_NUMBER = "licenceNumber";

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NaturalId
    @NotNull
    private long licenceNumber;

    @NotNull
    @Column(columnDefinition = "boolean default true")
    private boolean active;

    public Doctor() {
        // no-arg constructor required by Hibernate
    }

    public Doctor(String firstName, String lastName, long licenceNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.licenceNumber = licenceNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(long licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + this.getId() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", licenceNumber=" + licenceNumber +
                ", active=" + active +
                '}';
    }
}
