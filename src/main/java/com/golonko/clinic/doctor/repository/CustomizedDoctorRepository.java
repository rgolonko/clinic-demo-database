package com.golonko.clinic.doctor.repository;

import com.golonko.clinic.doctor.entity.Doctor;

interface CustomizedDoctorRepository {

    Doctor findByIdOrThrow(Long id);

}
