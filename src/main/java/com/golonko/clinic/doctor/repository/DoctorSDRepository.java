package com.golonko.clinic.doctor.repository;

import com.golonko.clinic.doctor.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorSDRepository extends JpaRepository<Doctor, Long>, CustomizedDoctorRepository {

}
