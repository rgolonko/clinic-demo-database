package com.golonko.clinic.doctor.repository;

import com.golonko.clinic.doctor.entity.Doctor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Repository
class CustomizedDoctorRepositoryImpl implements CustomizedDoctorRepository {

    private final DoctorSDRepository doctorSDRepository;

    public CustomizedDoctorRepositoryImpl(@Lazy DoctorSDRepository doctorSDRepository) {
        this.doctorSDRepository = doctorSDRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Doctor findByIdOrThrow(Long id) {
        return doctorSDRepository.findById(id).orElseThrow(() -> new NoSuchElementException(
                String.format("Doctor with %d not found.", id)
        ));
    }
}
