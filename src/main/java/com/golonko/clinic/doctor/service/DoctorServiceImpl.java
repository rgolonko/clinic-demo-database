package com.golonko.clinic.doctor.service;

import com.golonko.clinic.doctor.entity.Doctor;
import com.golonko.clinic.doctor.repository.DoctorSDRepository;
import com.golonko.clinic.visit.entity.Status;
import com.golonko.clinic.visit.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorSDRepository doctorSDRepository;
    private final VisitService visitService;

    @Autowired
    public DoctorServiceImpl(DoctorSDRepository doctorSDRepository, @Lazy VisitService visitService) {
        this.doctorSDRepository = doctorSDRepository;
        this.visitService = visitService;
    }

    @Override
    public List<Doctor> findAll() {
        return doctorSDRepository.findAll(Sort.by("firstName", "lastName"));
    }

    @Override
    public Doctor find(Long id) {
        return doctorSDRepository.findByIdOrThrow(id);
    }

    @Override
    public void create(Doctor doctor) {
        doctorSDRepository.save(doctor);
    }

    @Transactional
    @Override
    public Doctor update(Long id, String fistName, String lastName) {
        final Doctor doctor = this.find(id);
        doctor.setFirstName(fistName);
        doctor.setLastName(lastName);
        return doctor;
    }

    @Transactional
    @Override
    public void delete(Long id) {
        this.find(id).setActive(false);
        visitService.findAllByDoctorIdAndDateAndStatus(id, LocalDateTime.now(), Status.PENDING)
                .forEach(visit -> visit.setStatus(Status.NOT_ASSIGNED));
    }

    @Override
    public Doctor findOne(Long id) {
        try {
            return doctorSDRepository.getOne(id);
        } catch (EntityNotFoundException e) {
            throw new NoSuchElementException(
                    String.format("Doctor with %d not found", id));
        }
    }

}
