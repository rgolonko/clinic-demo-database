package com.golonko.clinic.doctor.service;

import com.golonko.clinic.doctor.entity.Doctor;

import java.util.List;

public interface DoctorService {

    List<Doctor> findAll();

    Doctor find(Long id);

    Doctor findOne(Long id);

    void create(Doctor doctor);

    Doctor update(Long id, String firstName, String lastName);

    void delete(Long id);
}
