# Clinic Demo Database

## General info
This is my first project with using **Spring Boot, Hibernate and MySql**. I started this project in order to:
* learn how to build database schema from entities,
* understand how JPA/Hibernate annotations work,
* communicate with the database by using SpringData and JPQL Queries,
* partially test repository layer by using Junit and AssertJ 

## Screenshots
*Database model created based on entities*

![Database Model](./misc/database-model.png)

## Status
**In Progress**

To-do list:
* controller layer
* security